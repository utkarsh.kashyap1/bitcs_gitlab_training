import axios from "axios";
import React, { useState } from "react";

//EDITING IN A NEW BRANCH

const App = () => {

  const [data, setData] = useState([]);
  const [todoValue,setTodoValue] = useState();

  const addTodo = () => {
    if(todoValue){
      let newData = data;
      newData.push({completed: true,id: '1234', title: todoValue, userId: 'any'});
      console.log(newData);
      setData(newData);
      newData = newData;

    } else {
      alert('fill the todoValue')
    }
  }
  const loadData = async() => {
    const response = await axios.get("https://jsonplaceholder.typicode.com/todos");
    console.log(response.data)
    if(response.data) {
      setData(response.data);
    }
  }

  const capatalise = () => {
    const newData = [];
    for(let i=0;i< data.length;i++){
      let newTitle = data[i].title.split(" ")
      let title = ""
      newTitle.map(word => (
        title += word[0].toUpperCase() + word.substring(1) + " "
      ));

      newData.push({completed:data[i].completed,id:data[i].id,title,userId: data[i].userId})
      setData(newData);
    }
  }

  const rearrange = () => {
    let completedTasks = data;
    let unCompletedTasks = data;

    completedTasks = completedTasks.filter(function(todo) {
      return todo.completed === true;
    });

    unCompletedTasks = unCompletedTasks.filter(function(todo) {
      return todo.completed === false;
    });

    for(let i=0; i<unCompletedTasks.length; i++){
      completedTasks.push(unCompletedTasks[i])
    }

    console.log(completedTasks);
    setData(completedTasks);
  }

  const clear = () => {
    let filterList = data;

    const filterFunction = (todo) => {
      return todo.completed === false;
    }
    
    filterList = filterList.filter(filterFunction)
    setData(filterList);
  }

  const deleteTodo = (id) => {
    let filterList = data;
    filterList = filterList.filter(function(todo){
      return todo.id !== id;
    })
    setData(filterList);
  }

  return (
    <div>
      <div>
        <button onClick={loadData}>LOAD</button>
        <input type='text' onChange={e=>setTodoValue(e.target.value)} />
        <button onClick={addTodo}>ADD TODO</button>
        {data.length>0 && (
          <div>
            <button onClick={capatalise} >Capatalise</button>
            <button onClick={rearrange} >Rearrange</button>
            <button onClick={clear} >Clear</button>
            {data.map(todo => (
              <div key={todo.id}>
                <span>{todo.id}</span>
                <span>{todo.title}</span>
                <span>{todo.userId}</span>
                <button onClick={() => deleteTodo(todo.id)}>X</button>
                <input type='checkbox' checked={todo.completed} />
              </div>
            ))}
          </div>
        )}
      </div>    
    </div>
  );
}

export default App;
